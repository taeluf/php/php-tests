<?php

namespace Tlf\Tester;

class Utility {

    static public function xdotoolRefreshFirefox($switchBackToWindow = false){
        $args = $switchBackToWindow ? ' y' : '';
        system(__DIR__.'/reload.sh'.$args);

        \Taeluf\Tester\Utility::xdotoolRefreshFirefox($switchBackToWindow);
    }


    static public function startOb(){
        ob_start();
        $ob_level = ob_get_level();
        return $ob_level;
    }
    static public function endOb($ob_level){
        $output = '';

        while (ob_get_level()>$ob_level){
            $output .= ob_get_clean()."\n";
        }
        $output .= ob_get_clean();
        return $output;
    }



    /**
     * dependency on Util added on Dec 8, 2022.
     * @param $file an absolute path to a file
     * @return null if no class found or a string with the `Qualified\Clazz\Name`
     */
    static public function getClassFromFile($file){
        $class = \Tlf\Util::getClassFromFile($file);
        return $class;
    }
/**
 * Copyright 2021 Reed Sutman, Taeluf
 * MIT License
 * See tluf.me/utils
 *
 * Please retain this notice
 *
 * This recursive file getting function actually came from tluf.me/liaison
 */
/**
 * Utility class to work with files & directories
 * @todo move to my util repo & depend upon this class.
 */

    /**
     * Get all files in a directory. Does not return directories
     *
     * @param $dir the directory to search in
     * @param $relativeTo A /root/path to remove from file paths returned
     * @param $endingWith only get files that end with the given string, like `.php` or `.md`, etc
     * @return array of files
     */
    static public function getAllFiles($dir,$relativeTo='', $endingWith=''){
        $fPathsRelTo = $relativeTo;
        if (!is_dir($dir))return [];
        $dir = str_replace(['///','//'],'/','/'.$dir.'/');
        $fPathsRelTo = $fPathsRelTo ? str_replace(['///','//'],'/','/'.$fPathsRelTo.'/') : null;
        $dh = opendir($dir);
        $allFiles = [];
        while ($file=readdir($dh)){
            if ($file=='.'||$file=='..')continue;
            if (is_dir($dir.$file)){
                $subFiles = self::getAllFiles($dir.'/'.$file,$fPathsRelTo,$endingWith);
                $allFiles = array_merge($allFiles,$subFiles);
                continue;
            }
            $path = str_replace(['///','//'],'/',$dir.'/'.$file);
            if ($fPathsRelTo!==null){
                $path = '/'.substr($path,strlen($fPathsRelTo));
            }
            if ($endingWith!=''
                &&substr($path,-strlen($endingWith))!=$endingWith)continue;
            $allFiles[] = $path;
        }
        return $allFiles;
    }

}
