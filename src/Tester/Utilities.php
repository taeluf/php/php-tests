<?php

namespace Tlf\Tester;

/**
 * Convenience methods to call from inside tests
 */
trait Utilities {

    protected int $php_major_version = -1;
    protected int $php_minor_version = -1;

    public function php_major_version(){
        if ($this->php_major_version!=-1)return $this->php_major_version;
        $version = phpversion();
        $version_int = substr($version,0,1);
        $php_version = (int)$version_int;
        $this->php_major_version = $php_version;
        return $php_version;
    }

    public function php_minor_version(){
        if ($this->php_minor_version!=-1)return $this->php_minor_version;
        $version = phpversion();
        $version_int = substr($version,0,1).substr($version,2,1);
        $php_version = (int)$version_int;
        $this->php_minor_version = $php_version;
        return $php_version;
    }

    protected function startOb(){
        return \Tlf\Tester\Utility::startOb();
    }
    protected function endOb($ob_level){
        return \Tlf\Tester\Utility::endOb($ob_level);
    }

    /**
     * get path to a file inside the current working directory
     */
    public function file($rel_path){
        return $this->cli->pwd.'/'.$rel_path;
    }

    /**
     * Delete all files in a directory
     * @param $dir the directory to delete
     * @param $recursive pass true for recursive deletion
     */
    public function empty_dir($dir, $recursive=false){
        if (!is_dir($dir))return;
        foreach (scandir($dir) as $f){
            if ($f=='.' || $f == '..')continue;
            if (is_file($dir.'/'.$f)){
                unlink($dir.'/'.$f);
            } else if ($recursive===true&&is_dir($dir.'/'.$f)){
                $this->empty_dir($dir.'/'.$f);
            }
        }
    }

    /**
     * Get a dumped version of the value (handles objects & arrays nicely
     */
    public function dump_value($value){
        if (is_callable($value)&&is_array($value)
            &&is_object($value[0]))return get_class($value[0]).'#'.spl_object_id($value[0]).'->'.$value[1];
        if (is_object($value))return get_class($value).'#'.spl_object_id($value);
        if (is_array($value))return array_map([$this, 'dump_value'], $value);

        return $value;
    }
}
