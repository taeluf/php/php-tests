<?php

namespace Tlf\Tester;

/**
 * Stuff that doesn't belong in other traits & isn't about the core execution of tests
 */
trait Other {

    protected $inverted = false;

    /**
     * called before your tests
     */
    public function prepare(){}
    /**
     * called after a class's tests are finished running
     */
    public function finish(){}

    /**
     * Call `$tester->test('TestName')->compare($target,$actual)` to run a sub-test inside a test in your class
     * Chaining is not required
     * 
     * @export(Usage.test)
     */
    protected function test($subTestName){
        echo "\n\n****$subTestName****";
        return $this;
    }

    /**
     * Disable the current test
     */
    public function disable(){
        echo "  TEST DISABLED  ";
        $this->enabled = false;
    }

    /**
     * Invert test passage so failing tests actually passes them
     */
    public function invert(){
        $this->inverted = !$this->inverted;
        return $this->inverted;
    }


    public function print_r(array $array){
        print_r(
            $this->printable_array($array)
        );
    }

    /**
     * get a printable version of an array (replace objects with their class names)
     */
    public function printable_array(array $array): array {
        $out = [];
        foreach ($array as $key=>$value){
            if (is_object($value))$value = get_class($value).'#'.spl_object_id($value);
            if (is_array($value))$value = $this->printable_array($value);
            $out[$key] = $value;
        }
        return $out;
    }

    public function handleDidPass(bool $didPass, $print=true){
        $extra='';
        if ($this->inverted){
            $didPass = !$didPass;
            $extra=" (inverted)";
        }
        if ($print){
            echo "\n\n";
            if ($didPass)echo "+++pass+++$extra";
            else echo "---fail---$extra";
            echo "\n";
        }
        $passStr = $didPass ? 'pass': 'fail';
        $this->assertions[$passStr]++;
        return $didPass;
    }


    public function targetVsActualOutput($target, $actual){

        if (!is_string($target))$target = $this->comparisonOutput($target);
        if (!is_string($actual))$actual = $this->comparisonOutput($actual);

        // echo "\n";
        echo "Target:\n{$target}";
        echo "\n--\n";
        echo "Actual:\n{$actual}";
        echo "\n--------\n";
    }


    public function comparisonOutput($value){
        if (is_object($value)){
            return "Object of class ".get_class($value);
        }
        if (is_array($value)){

            array_walk_recursive($value, function(&$value){
                $value = str_replace(["\r","\n"],['\r','\n'], $value??'');
            });

            $pr = var_export($value, true);
            $maxLen= 1000;
            
            if (($this->options['prettyPrintArray'][0]??null)=='true'){
                $oneLine = $pr;
                $maxLen = 1500;
            } else {
                $oneLine = implode(" ",array_map('trim',explode("\n",$pr)));
                $oneLine = str_replace('\\\\n', '\n', $oneLine);
            }
            $value = substr($oneLine,0,$maxLen);
            if (strlen($oneLine)>$maxLen)$value .= '...';

            return $value;
        }

        if ($value===true)return 'true';
        else if ($value===false)return 'false';

        return $value;
    }
}
