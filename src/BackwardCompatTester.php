<?php

namespace Taeluf;

/**
 * @todo delete this class
 */
class Tester extends \Tlf\Tester {

    public function backward_compatability(){
        parent::backward_compatability();

        echo "\n\n-------NOTICE--------";
        echo "\n       class Taeluf\\Tester is now class Tlf\\Tester     ";
        echo "\n     update ". get_class($this)." to extend from \\Tlf\\Tester";
        echo "\n     \\Taeluf\\Tester will be removed in v0.4";
        echo "\n";
    }
}
