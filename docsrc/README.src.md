# Tester - Unit Testing for Php
Unit testing library. @hard_link(https://phpunit.de, phpunit) has more features & community.

## Install
@template(php/composer_install, taeluf/tester)

## Run Tests | CLI Interface
Call `phptest` or `vendor/bin/phptest` from your project's root dir. 

A globally installed `phptest` will execute the `vendor/bin/phptest` copy if it exists.

```bash
phptest # simply run tests

phptest init # Create test dir & example test files

phptest server # launch test server using default settings
phptest server server_name # launch a configured server
phptest server -host.override "https://example.com" # typically server tests will use a localhost address. This overrides the host in all tests, even if you have a multi-server setup.

phptest -test TestName # Run one test; display all output for the named test
phptest -test TestName -test Another Test # Run all named tests
phptest -class ClassName # Run all tests for a class. Do not include namespace. Can include multiple classes.

phptest -custom_key SomeValue # For any tests that allow custom input from CLI.
```

Other Options:
```bash
-class ClassName -test TestName # Only run TestName for the given ClassName
-prettyPrintArray true # Not sure if this works. To print array comparisons on multiple lines. By default output is condensed.
-set_error_handler false # Not sure if this works. Enable the built-in error handler that causes all warnings, etc, to throw an exception.
-bench.threshold 0.001 # Only print benchmark for tests taking longer than 1 ms to run. `0.001` is configurable.
```

## Example Test class
See the available assertions below
```php
@file(test/input/Runner/run/Test.php)
```

## Sample config
`test/config.json` is optional, but recommended. Any values not in your `config.json` will be filled by the defaults below.

Default `config.json`:
```json
@file(src/defaults.json)
```

## Assertions
For docs/details, see @see_file(src/Tester/Assertions.php)

```php
isInstanceOf($object, $shouldBe)
is_object($object)
is_false($value)
is_true($value)
str_contains($str, $target, ...$strings)
str_not_contains(string $str, $target, ...$strings)
compare($target, $actual,$strict=false)
compare_raw($target, $actual, $strict=false)
compare_arrays($target, $actual, $strict=false)
compare_objects($target, $actual, $strict=false)
compare_json($target, $actual, $strict=false)
str_contains_lines($str, $target)
compare_lines($target, $actual)
compare_dump($target, $actual)
```

There is also a magic `__call()` which will invoke an existing PHP function and use it's return value as an assertion. 

For example, `$this->in_array('value', $array_to_test);` will `pass` if PHP's `in_array()` method returns `true`

## Alternate Installation
This also requires @easy_link(tlf, code-scrawl) for development, which is not handled by this script.  
copy+paste this into a bash terminal
@template(bash/install, phptest, code/phptest)


