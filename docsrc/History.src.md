
---

# v0.2 Status.md (nov 24, 2021)

## Latest
- add handling for `testDirs` config so only php files in the configured test dirs are automatically `include`d
- add `set_error_handler` config. set `false` to disable tester's built in handler.
- set `$this->options` BEFORE calling `$this->prepare()`
- add `dir.require` config which accepts an array & will `require_once` each `.php` file in the directory (NOT recursive)
- Print newlines as literal `\n` in array output
- add `file.require` option, which takes an array & automatically `require`s any files in that array (relative to the current working directory)
- add `results.writeHtml` config. Set `false` to disable writing an html results file
- Add automatic assertions, where you can just call any existing function & it is treated as an assertion. Except you call it like `$this->array_key_exists(...)` to use `array_key_exists(...)` as an assertion.
- `phptest -test TestName` now ONLY runs the named test. You might be able to specify multiple `-test`s... but I haven't tested it.
- Create `Assertions` trait to group assertions
- fixed bug with a throw/catch not marking test as passed
- Different location for config files allowed
- Improved cli output
- Added `-test TestName` option to cli invocation to display extended output of a single test in cli
- Added `Databasing` trait to make some db operations a little easier.
