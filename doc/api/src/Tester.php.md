<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Tester.php  
  
# class Tlf\Tester  
Base class for test classes  
See source code at [/src/Tester.php](/src/Tester.php)  
  
## Constants  
  
## Properties  
- `protected $catchers = [];`   
- `protected $assertions = ['pass'=> 0, 'fail'=>0];` Comparisons from a single test. Should be reset between tests.  
- `protected $enabled = true;`   
- `protected $options = [];`   
- `public $cli = null;` The cli class used to run the tests  
- `public string $current_test = null;` The string name of the method being called for the current test. Like `"testSomething"`  
  
## Methods   
- `public function __construct(array $options=[], $clinull)`   
- `public function throwError($errno, $errstr, $errfile, $errline)`   
- `public function onBeforeTest()`   
- `public function backward_compatability()`   
- `public function get_test_methods()` Get array of test methods names  
- `public function get_test_name($method_name): string` get a readable name from a test method name  
- `public function run_test_method($method)`   
- `public function print_test_results($test)`   
- `public function run()` Run tests  
  
  
