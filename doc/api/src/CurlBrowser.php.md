<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/CurlBrowser.php  
  
# class Tlf\Tester\CurlBrowser  
  
See source code at [/src/CurlBrowser.php](/src/CurlBrowser.php)  
  
## Constants  
  
## Properties  
- `public string $default_host;` The default host to connect to, such as http://localhost:3183  
- `public array $responses = [];` Array of responses as built by `curl_get_response()`, in the order they were received  
  
## Methods   
- `public function __construct(string $default_host = null)`   
- `public function __toString()` the body of the last response  
- `public function last()`   
- `public function follow()` Follow the redirect sent with the last request  
- `public function get($path, $params=[], $headers[], $curl_opts  [])`   
- `public function post($path, $params=[], $headers[], $files[], $curl_opts  [])` @beta  
  
- `public function make_url(string $url, array $params=[]): string`   
- `public function parse_cookie_header($header_value): array`   
- `public function curl_add_files($ch, array $files, array $params)`   
- `public function curl_get_response($ch)`   
  
