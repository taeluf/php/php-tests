<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Runner.php  
  
# class Tlf\Tester\Runner  
  
See source code at [/src/Runner.php](/src/Runner.php)  
  
## Constants  
  
## Properties  
- `public $server_port = [];`   
  
## Methods   
- `public function setup_commands(\Tlf\Cli $cli)` Load all commands onto the cli  
- `public function initialize(\Tlf\Cli $cli, array $args): array` Initialize the runner and return array of cli arguments,   
  
- `public function load_configs(\Tlf\Cli $cli, string $dir, array $args)` Load config file from disk  
  
- `public function get_server_host(string $name='main'): string` get the server host   
- `public function get_host_port(string $server_name): int`   
- `public function print_config(\Tlf\Cli $cli, array $args)`   
- `public function start_server(\Tlf\Cli $cli, array  $args)` Start a localhost server for testing  
- `public function init(\Tlf\Cli $cli, array $args)` Copies sample test files into `getcwd().'/test'`  
- `public function require_directory(string $path)` Require every php file within a directory  
- `public function run_dir(\Tlf\Cli $cli, array $args): array` executes all tests inside test directories (config `dir.test`)  
  
  
