<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/FakeServer.php  
  
# class Tlf\Tester\FakeServer  
Used to run a php script as if an HTTP request had been made.  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function send_request(string $deliver_file, string $url, string $method = "GET", array $params  []): string` Set up the $_SERVER, $_GET, and $_POST global variables accordingly, require the $deliver_file, and return its output.   
  
