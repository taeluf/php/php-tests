<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/Runner/run/Test.php  
  
# class Tlf\Tester\Test\Runner\NestedTest  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function prepare()` called before tests are run  
- `public function testAnything()` Test methods must prefix with `test`  
  
