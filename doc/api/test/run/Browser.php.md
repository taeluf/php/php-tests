<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Browser.php  
  
# class Tlf\Tester\Test\Browser  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testPostFile()`   
- `public function testBrowserPost()`   
- `public function testBrowserFollow()`   
- `public function testBrowserGetParam()`   
- `public function testBrowserGet()`   
  
