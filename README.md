<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Tester - Unit Testing for Php  
Unit testing library. [phpunit](https://phpunit.de) has more features & community.  
  
## Install  
```bash  
composer require taeluf/tester v0.3.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/tester": "v0.3.x-dev"}}  
```  
  
  
## Run Tests | CLI Interface  
Call `phptest` or `vendor/bin/phptest` from your project's root dir.   
  
A globally installed `phptest` will execute the `vendor/bin/phptest` copy if it exists.  
  
```bash  
phptest # simply run tests  
  
phptest init # Create test dir & example test files  
  
phptest server # launch test server using default settings  
phptest server server_name # launch a configured server  
phptest server -host.override "https://example.com" # typically server tests will use a localhost address. This overrides the host in all tests, even if you have a multi-server setup.  
  
phptest -test TestName # Run one test; display all output for the named test  
phptest -test TestName -test Another Test # Run all named tests  
phptest -class ClassName # Run all tests for a class. Do not include namespace. Can include multiple classes.  
  
phptest -custom_key SomeValue # For any tests that allow custom input from CLI.  
```  
  
Other Options:  
```bash  
-class ClassName -test TestName # Only run TestName for the given ClassName  
-prettyPrintArray true # Not sure if this works. To print array comparisons on multiple lines. By default output is condensed.  
-set_error_handler false # Not sure if this works. Enable the built-in error handler that causes all warnings, etc, to throw an exception.  
-bench.threshold 0.001 # Only print benchmark for tests taking longer than 1 ms to run. `0.001` is configurable.  
```  
  
## Example Test class  
See the available assertions below  
```php  
<?php  
  
namespace Tlf\Tester\Test\Runner;  
  
class NestedTest extends \Tlf\Tester {  
  
    /** called before tests are run */  
    public function prepare(){}  
  
    /** Test methods must prefix with `test` */  
    public function testAnything(){  
        $this->compare(true,true);  
    }  
}  
```  
  
## Sample config  
`test/config.json` is optional, but recommended. Any values not in your `config.json` will be filled by the defaults below.  
  
Default `config.json`:  
```json  
{  
    "dir.test":["test/run"],  
    "dir.exclude":[],  
    "file.require":[],  
    "//file.log_to":"test/tests.log",  
  
    "results.writeHtml":false,  
  
    "bench.threshold": 0.0001,  
  
    "server.main": "main",  
    "server":{  
        "main": {   
            "dir":"test/Server",  
            "bootstrap":"bootstrap.php",  
            "deliver":"deliver.php",  
            "host": "http://localhost"  
        }  
    }  
}  
```  
  
## Assertions  
For docs/details, see [src/Tester/Assertions.php](/src/Tester/Assertions.php)  
  
```php  
isInstanceOf($object, $shouldBe)  
is_object($object)  
is_false($value)  
is_true($value)  
str_contains($str, $target, ...$strings)  
str_not_contains(string $str, $target, ...$strings)  
compare($target, $actual,$strict=false)  
compare_raw($target, $actual, $strict=false)  
compare_arrays($target, $actual, $strict=false)  
compare_objects($target, $actual, $strict=false)  
compare_json($target, $actual, $strict=false)  
str_contains_lines($str, $target)  
compare_lines($target, $actual)  
compare_dump($target, $actual)  
```  
  
There is also a magic `__call()` which will invoke an existing PHP function and use it's return value as an assertion.   
  
For example, `$this->in_array('value', $array_to_test);` will `pass` if PHP's `in_array()` method returns `true`  
  
## Alternate Installation  
This also requires [code-scrawl](https://tluf.me/code-scrawl) for development, which is not handled by this script.    
copy+paste this into a bash terminal  
```bash  
pwd="$(pwd)";  
command="phptest"  
downloadDir=~/.gitclone  
mkdir -p "$downloadDir"  
cd "$downloadDir"  
git clone https://gitlab.com/taeluf/php/php-tests.git ${command}   
echo "alias ${command}=\"${downloadDir}/${command}/code/phptest\"" >> ~/.bashrc  
chmod ug+x "${downloadDir}/${command}/code/phptest"  
cd "$pwd";  
source ~/.bashrc  
echo "You can now run \`${command}\`"  
```  
  
  
  
