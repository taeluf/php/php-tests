<?php

namespace Tlf\Tester\Test;

class Browser extends \Tlf\Tester {

    public function testPostFile(){
        $browser = new \Tlf\Tester\CurlBrowser($this->get_server());
        $browser->post('/file/', ['postme'=>'fun'],[], ['file'=>$this->file('test/input/Browser/file.txt')]);

        $this->compare('file.txt:this is text', ''.$browser);
    }

    public function testBrowserPost(){
        $browser = new \Tlf\Tester\CurlBrowser($this->get_server());
        $browser->post('/post/', ['postme'=>'fun']);

        $this->compare('postme:fun', ''.$browser);
    }

    public function testBrowserFollow(){
        $browser = new \Tlf\Tester\CurlBrowser($this->get_server());
        $browser->get('/go-home/');
        $this->compare('', $browser.'');

        $browser->follow();
        
        $this->compare('browser-test', $browser.'');
    }

    public function testBrowserGetParam(){

        $browser = new \Tlf\Tester\CurlBrowser($this->get_server());
        $browser->get('/param/', ['param'=>'test']);

        $this->compare('test', $browser.'');
    }

    public function testBrowserGet(){

        $browser = new \Tlf\Tester\CurlBrowser($this->get_server());
        $browser->get('/browser/');

        $this->compare('browser-test', $browser.'');
    }
}
