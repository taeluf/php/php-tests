<?php

namespace Tlf\Tester\Test;

class Exceptions extends \Tlf\Tester {

    public function testTriggerError(){

        if ($this->php_major_version()<8){
            $this->catch('Exception')
                 ->containing('Undefined index: cat')
             ;
        } else {
            $this->catch('Exception')
                 ->containing('Undefined array key "cat"')
             ;
        }
        $array = [];
        try {
            $cat = $array['cat'];
        } catch (\Exception $e){
            $this->throw($e);
        }
    }

    public function testThrowException(){
        $this->catch('Exception')
             ->containing('Undefined index: cat')
         ;
        try {
            throw new \Exception('Undefined index: cat');
        } catch (\Exception $e){
            $this->throw($e);
        }
    }

}
